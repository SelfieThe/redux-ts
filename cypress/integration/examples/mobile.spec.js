/// <reference types="cypress" />

import { listPages } from "../../fixtures/listPages";
// import { login } from "../login";


const sizes = [
  "ipad-2",
  "ipad-mini",
  "iphone-xr",
  "iphone-x",
  "iphone-6+",
  "iphone-se2",
  "iphone-8",
  "iphone-7",
  "iphone-6",
  "iphone-5",
  "iphone-4",
  "iphone-3",
  "samsung-s10",
  "samsung-note9",
];
context("Mobile", () => {
  sizes.forEach((size) => {
    // login();
    beforeEach(() => {
      cy.viewport(size);
    });
    it("setCow", () => {
      cy.get(
        ".MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root > path"
      ).click();
      cy.get(".makeStyles-drawerContainer-5").contains("Тут кнопка из header").click();
      cy.contains("тут кнопка из appmenu").click();
      setCow();
    });
    listPages.forEach((page) => {
      it(page, () => {
        cy.get(
          ".MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root > path"
        ).click();
        cy.get(".makeStyles-drawerContainer-5").contains(page).click();
        cy.viewport(size);
      });
    });
  });
});
