describe('Cypress Login Test', () => {
  it('None-existent login test', () => {
    cy.fixture('login').then(data => {
      cy.log('Переход на страницу авторизации')
      cy.visit(data.main_url, { timeout: 120000 })

      cy.log('Ввод несуществующего логина')
      cy.get('input[id="input-login"]')
            .type(data.none_existent_login)

      cy.log('Клик по кнопке войти')
      cy.get('button[id="login-button"]')
            .click()

      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(2000);

      cy.log('Ввод несуществующего пароля')
      cy.get('input[id="input-password"]')
            .type(data.none_existent_password)

      cy.log('Клик по кнопке войти')
      cy.get('button[id="login-button"]')
            .click()

      cy.log('Проверка что появился элемент выдающий ошибку')
      cy.get('div[class="MuiSnackbar-root MuiSnackbar-anchorOriginBottomCenter"]')
            .should('exist')


      cy.log('Переход на страницу авторизации')
      cy.visit(data.main_url, { timeout: 120000 })

      cy.log('Ввод существующего логина')
      cy.get('input[id="input-login"]')
            .type(data.existent_login)

      cy.log('Клик по кнопке войти')
      cy.get('button[id="login-button"]')
            .click()

      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(2000);

      cy.log('Ввод существующего пароля')
      cy.get('input[id="input-password"]')
            .type(data.existent_password)

      cy.log('Клик по кнопке войти')
      cy.get('button[id="login-button"]')
            .click()

      cy.setCookie('token', '123')
      cy.reload()

      cy.log('Проверка что появился элемент выдающий ошибку')
      cy.get('div[class="App_root__17oKS"]')
            .should('exist')
    })
  })
})
