import React             from 'react'
import Grid              from '@mui/material/Grid'
import Typography        from '@mui/material/Typography'
import { LineChart     } from 'recharts'
import { CartesianGrid } from 'recharts'
import { Line          } from 'recharts'
import { Tooltip       } from 'recharts'
import { XAxis         } from 'recharts'

interface ChartLineConfig {
  Key?: number,
  Width?: number,
  Height?: number,
  Data?: any,  
}

export const ChartLine = (Config: ChartLineConfig, ChartData: any, Styles: any) => {
  const DefaultWidth  = 400
  const DefaultHeight = 300
  return (
    <Grid className = {Styles.HorizontalCenter}>
      {ChartData.length > 0 ? 
      <LineChart
        key    = { Config.Key    }
        data   = {ChartData}//{ Config.Data   }
        margin = {{ top: 0, right: 20, left: 0, bottom: 10      }}
        width  = { Config.Width  ? Config.Width  : DefaultWidth  }
        height = { Config.Height ? Config.Height : DefaultHeight }        
      >
        <Line 
          type    = "monotone" 
          dataKey = "count" 
          stroke  = "#8884d8" 
        />
        <XAxis dataKey="date" />
        <CartesianGrid 
          stroke          = "#ccc" 
          strokeDasharray = "5 5" 
        />
        <Tooltip />
      </LineChart> 
      : 
      <Grid 
        style={{ 
          width:          Config.Width  ? Config.Width  : DefaultWidth, 
          height:         Config.Height ? Config.Height : DefaultHeight, 
          display:        'flex', 
          justifyContent: 'center', 
          alignItems:     'center'
        }}>
          <Typography>Нет данных</Typography>
      </Grid>}
    </Grid>
  )
}