import React     from 'react'
import TextField from '@mui/material/TextField'

export const DatePickers = (ConfigFirsDate: any, ConfigLastDate: any, Styles: any) => {
  return (
    <form 
      className  = { Styles.ContainerDatePicker } 
      noValidate
    >
      <TextField
        id              =    "firstdate"
        label           =    "FirstDate"
        type            =    "date"
        value           = {  ConfigFirsDate.firstDate             }
        onChange        = {  ConfigFirsDate.handleFirstDateChange }
        className       = {  Styles.TextFieldDatePicker           }
        InputLabelProps = {{ shrink: true                        }}
      />
      <TextField
        id              =    "lastdate"
        label           =    "LastDate"
        type            =    "date"        
        value           = {  ConfigLastDate.lastDate             }
        onChange        = {  ConfigLastDate.handleLastDateChange }
        className       = {  Styles.TextFieldDatePicker          }
        InputLabelProps = {{ shrink: true                       }}
      />
    </form>
  )
}