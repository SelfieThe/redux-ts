import React      from "react"
import Checkbox   from "@mui/material/Checkbox"
import Typography from "@mui/material/Typography"

export const MonthChecker = (Config: any, Styles: any) => {
  return (
    <>
      <Checkbox
        checked={Config.stateMonth}
        onChange={()=>Config.handleChangeMonthState()}
      />
      <Typography className={Styles.VerticalCenter}>По месяцам</Typography>
    </>
      
  )
}