import   React            from 'react'
import   Grid             from "@mui/material/Grid"
import   Typography       from '@mui/material/Typography'
import { XAxis          } from 'recharts'
import { YAxis          } from 'recharts'
import { CartesianGrid  } from 'recharts'
import { Tooltip        } from 'recharts'
import { BarChart       } from 'recharts'
import { Bar            } from 'recharts'

interface ChartBarConfig {
  Key?: number,
  Width?: number,
  Height?: number,
  Data?: any,  
}

export const ChartBar = (Config: ChartBarConfig, ChartData: any, Styles: any) => {
  const DefaultWidth  = 400
  const DefaultHeight = 300
  return (
    
    <Grid className = {Styles.HorizontalCenter}>
      {ChartData.length > 0 ? 
      <BarChart
        key    = { Config.Key }
        data   = { ChartData  }
        margin = {{ top: 0, right: 20, left: 0, bottom: 10      }}
        width  = { Config.Width  ? Config.Width  : DefaultWidth  }
        height = { Config.Height ? Config.Height : DefaultHeight }        
      >
        <CartesianGrid 
          strokeDasharray = "5 5" 
        />

        <YAxis 
          tickCount = {3}
        />
        <Tooltip />
        <Bar 
          type    = "monotone" 
          dataKey = "count" 
          fill    = "#8d230f" 
        />
        <XAxis dataKey="date" />
      </BarChart> 
      : 
      <Grid 
        style={{ 
          width:          Config.Width  ? Config.Width  : DefaultWidth, 
          height:         Config.Height ? Config.Height : DefaultHeight, 
          display:        'flex', 
          justifyContent: 'center', 
          alignItems:     'center'
        }}>
          <Typography>Нет данных</Typography>
      </Grid>}
    </Grid>
  )
}
