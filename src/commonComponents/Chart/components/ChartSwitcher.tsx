import React            from 'react'
import Radio            from '@mui/material/Radio'
import RadioGroup       from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import Grid             from '@mui/material/Grid'

export const ChartSwitcher = (ConfigChartSwitcher: any, Styles: any) => {
  return (
    <Grid className={Styles.HorizontalCenter}>
      <RadioGroup  
        value={ConfigChartSwitcher.typeChart} 
        onChange={ConfigChartSwitcher.handleChangeTypeChart}
        className={Styles.ChartRadioGroup}       
      > 
        <Grid className={Styles.HorizontalCenter}>
          <FormControlLabel 
            value   =   "Line" 
            control = { <Radio /> } 
            label   =   "Линейный" 
          />
          <FormControlLabel 
            value   =   "Bar" 
            control = { <Radio /> } 
            label   =   "Гистограмма" 
          />  
        </Grid>    
      </RadioGroup> 
    </Grid>
  )
}
