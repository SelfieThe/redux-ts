import React                           from 'react'
import Box                             from '@mui/material/Box'
import Card                            from '@mui/material/Card'
import Grid                            from '@mui/material/Grid'
import Typography                      from '@mui/material/Typography'
import { Controller, GetUrl, GetData } from './controller/Controller'
import { ChartSwitcher               } from './components/ChartSwitcher'
import { DatePickers                 } from './components/ChartDatePickers'
import { ChartLine                   } from './components/ChartLine'
import { ChartBar                    } from './components/ChartBar'
import { MonthChecker                } from './components/ChartMonthChecker'

export interface MainConfig {
  Id          : number,  
  Request?    : number,
  Width?      : number,
  DatePicker  : boolean,
  SwitchChart : boolean,
  Month       : boolean,
  Title       : string,
  TypeChart   : string,
  Group?      : number,
  Get         : string,
  CowId?      : number,
  Date?       : string
}
interface ChartConfig {
  Props       : any,
}

export const Chart = (Config: ChartConfig) => {
  const ControllerMonthChecker: any = Controller().ControllerMonthChecker
  const ControllerFirstDate: any    = Controller().ControllerFirstDate
  const ControllerLastDate: any     = Controller().ControllerLastDate
  const ControllerTypeChart: any    = Controller().ControllerTypeChart
  
  // Config.Props.Date ? ControllerFirstDate.firstDate = Config.Props.Date.time_start : null

  const url                         = GetUrl(Config.Props.Get? Config.Props.Get : 'byDate', Config.Props.Request, ControllerFirstDate.firstDate, ControllerLastDate.lastDate, ControllerMonthChecker.stateMonth, Config.Props.Group, Config.Props.DatePicker, Config.Props.CowId)
  const chartData                   = GetData(url, ControllerFirstDate.firstDate, ControllerLastDate.lastDate, ControllerMonthChecker.stateMonth, Config.Props.CowId)
  console.log(ControllerMonthChecker)
  console.log(ControllerFirstDate)
  console.log(ControllerLastDate)
  console.log(ControllerTypeChart)
  console.log('////////////////////')
  return (
    
    <Box id={Config.Props.id} className={Config.Props.styles.Chart}>
      <Card variant="outlined">
        <Grid className={Config.Props.styles.HorizontalEnd}>
          <Grid className={Config.Props.styles.VerticalCenter}>
            <Typography style={{marginRight: '10%'}}>
              {Config.Props.Title}
            </Typography>
              {Config.Props.Month ?
              MonthChecker(ControllerMonthChecker, Config.Props.styles)
            : null}
          </Grid>
        </Grid>
        
        {Config.Props.DatePicker ? 
          DatePickers(ControllerFirstDate, ControllerLastDate, Config.Props.styles)
        : null}

        {Config.Props.TypeChart === "Multi" && ControllerTypeChart.typeChart === "Line" ? 
          ChartLine(Config.Props.ChartLine, chartData, Config.Props.styles)
        : <></>}

        {Config.Props.TypeChart === "Multi" && ControllerTypeChart.typeChart === "Bar"?  
          ChartBar(Config.Props.ChartLine, chartData, Config.Props.styles)
        : <></>}

        {Config.Props.TypeChart === "Line" ? 
          ChartLine(Config.Props.ChartLine, chartData, Config.Props.styles)
        : <></>}

        {Config.Props.TypeChart === "Bar" ?  
          ChartBar(Config.Props.ChartLine, chartData, Config.Props.styles)
        : <></>}

        {Config.Props.SwitchChart ?          
          ChartSwitcher(ControllerTypeChart, Config.Props.styles)
        : null}
      </Card>
    </Box>
  )
}

export function CreateChart(config: MainConfig){
  return (
    <Chart
      Props={config}
    />  
  )
}