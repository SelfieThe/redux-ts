import React            from "react"
// import { fetchRequest } from "../../../../commonFunctions/requests/apiInstans"
// import { reqStore     } from "../../../../commonFunctions/requests/RequestStore"

export function Controller() {
  const [stateMonth, setStateMonth] = React.useState(false)

  const handleChangeMonthState = () => {
    setStateMonth(!stateMonth)
  }
  const ControllerMonthChecker: any = { stateMonth, handleChangeMonthState }

//////////////////

  const currentDate:Date            = new Date();
  const cDay                        = currentDate.getDate()
  const cMonth                      = currentDate.getMonth() + 1
  const cYear                       = currentDate.getFullYear()
  const [ firstDate, setFirstDate ] = React.useState(`${cYear}-0${cMonth}-${cMonth>9? '':'0'}${cDay-cDay+1}`)
  const [ lastDate,  setLastDate  ] = React.useState(`${cYear}-0${cMonth}-${cDay>9? "":"0"}${cDay}`)
  const currDate                    = new Date(lastDate.replace("-", " ").replace("-", " "))
  const today                       = new Date(currDate.getFullYear(), currDate.getMonth(), currDate.getDate()).valueOf()
  const afterCurrDate               = new Date(firstDate.replace("-", " ").replace("-", " "))
  const lastToday                   = new Date(afterCurrDate.getFullYear(), afterCurrDate.getMonth(), afterCurrDate.getDate()).valueOf()
  
  const handleFirstDateChange = (e:any) => {
    const transformDate = new Date(e.target.value.replace("-", " ").replace("-", " "))    
    const formattedDate = new Date(transformDate.getFullYear(), transformDate.getMonth(), transformDate.getDate()).valueOf()
    
    if (formattedDate < today - 86400000) { // 24*60*60*1000
      //'раньше чем вчера'
      setFirstDate(e.target.value)
    } else if (formattedDate < today) {
      //вчера'
      setFirstDate(e.target.value)
    } else {
      //'сегодня или потом'
      setFirstDate(lastDate)      
    }
  }

  const handleLastDateChange = (e:any) => {  
    const transformDate = new Date(e.target.value.replace("-", " ").replace("-", " "))    
    const formattedDate = new Date(transformDate.getFullYear(), transformDate.getMonth(), transformDate.getDate()).valueOf()

    if (formattedDate > lastToday - 86400000) { // 24*60*60*1000
      //'позже чем завтра'      
      setLastDate(e.target.value)
    } else if (formattedDate > lastToday) {
      //завтра
      setLastDate(e.target.value)
      
    } else {
      //'сегодня или раньше'      
      setLastDate(lastDate)      
    }   
  }

  const ControllerFirstDate = { firstDate, handleFirstDateChange }
  const ControllerLastDate  = { lastDate, handleLastDateChange   }

/////////////////

  const [typeChart, setTypeChart] = React.useState('Line')
  const handleChangeTypeChart = (e: any) => {    
    setTypeChart(e.target.value);
  }

  const ControllerTypeChart = { typeChart, handleChangeTypeChart }
//////////////////
  return {ControllerMonthChecker, ControllerFirstDate, ControllerLastDate, ControllerTypeChart}
}

export const GetUrl = (Type: string, ReqStoreNumber: number, FirstDate: string, LastDate: string, StateMonth: boolean, Group: any, DatePicker: boolean, Id: number) => {
  const [ url, setUrl ] = React.useState({})//{url: '', type: reqStore[ReqStoreNumber].type}
  const formattedUrl = url
  // Type === 'byDate'  ? formattedUrl.url = `${reqStore[ReqStoreNumber].url}time_start=${FirstDate} 00:00:00&time_end=${LastDate} 23:59:59${StateMonth ? '&interval=months' : ''}` : null
  // Type === 'byGroup' ? formattedUrl.url = `${reqStore[ReqStoreNumber].url}${Group ? Group : 0}${FirstDate || DatePicker ? `&time_start=${FirstDate} 00:00:00&time_end=${LastDate} 23:59:59` : ''}` : null
  // Type === 'byCowId' ? formattedUrl.url = `${reqStore[ReqStoreNumber].url}${Id ? Id : 0}&time_start=${FirstDate} 00:00:00&time_end=${LastDate} 23:59:59${StateMonth ? '&interval=months' : ''}` : null

  return url
}

export const GetData = (Url: any, FirstDate: string, LastDate: string, StateMonth: boolean, CowId: number) => {
  const [ chartData, setChartData ]: any = React.useState([
    {count: 1,  date: '01-01-2022'},
    {count: 4,  date: '02-01-2022'},
    {count: 25, date: '03-01-2022'},
    {count: 10, date: '04-01-2022'},
  ])
  // React.useEffect(() => {                    
  //   fetchRequest(Url).then((res: any) => {      
  //     if(res){
  //       setChartData(JSON.parse(res))
  //     }
  //   }) 
  //   return () => {
  //     setChartData([])
  //   } 
  // }, [Url, FirstDate, LastDate, StateMonth, CowId])
  return chartData
}
