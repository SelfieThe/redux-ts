import React from 'react';
import Grid from '@mui/material/Grid';
import { Routes, Route } from 'react-router-dom'

import './App.css';
import { Home } from './pages/home/Home';
import { Page } from './pages/Page';
import { Header } from './app/header/Header';
import { MyTable } from './pages/table/Table'
import { MySlider } from './pages/slider/Slider';

function App() {
  return (
    <Grid className="App">
      <Grid style={{display: 'flex', justifyContent: 'center'}}>
        <Header/>
      </Grid>
      <Routes>
        <Route path='/' element={<Home />}/>
        <Route path='/page' element={<MyTable />}/>
        <Route path='/page1' element={<MySlider />}/>
      </Routes>
    </Grid>
  );
}

export default App;
