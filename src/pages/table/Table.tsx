import Grid from "@mui/material/Grid"
import React from "react"



export const MyTable = () => {
  const [filterInput, setFilterInput] = React.useState("")
  const HeadTable = ['Head1', 'Head2', 'Head3', 'Head4', 'Head5']
  const DataTable = [
        ['12', '2', '3', '4', '5'],
        ['a', 'b', 'c', 'd', 'e'],
        ['1', '2', '3', '4', '5'],
        ['a', 'b', 'c', 'd', 'e'],
        ['14', '2', '3', '4', '5']
  ]
    
  const handleFilterChange = (e:any) => {
    const value = e.target.value || undefined;
    setFilterInput(value);
  }
  
  return (
    <Grid container direction='column' >
      <Grid item>
      <input
        value={filterInput}
        onChange={handleFilterChange}
        placeholder={"Search by Head1"}
      />
      </Grid>
      <Grid item style={{display:'flex', justifyContent:'center'}}>
      <table>
        <thead>
          <tr >
            {HeadTable.map(headerGroup => (
              
              <th key={headerGroup}>{headerGroup}</th>
              
            ))}
          </tr>
        </thead>
        <tbody>
          {DataTable.map((row, i) => {
            
            return (
              row[0].match(filterInput) ? 
              <tr key={i}>
                {row.map((cell, i) => {                  
                  return <td key={cell}>{cell}</td>;
                })}
              </tr> : null
            );
          })}
        </tbody>
    </table>
    </Grid>
  </Grid>
  )
  
}