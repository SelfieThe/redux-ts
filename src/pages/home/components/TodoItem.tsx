import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { DeleteButton } from '../../../app/animation/deleteButton/DeleteButton';
import { Loading } from '../../../app/animation/loading/Loading';
import { toggleComplete, deleteTodo } from '../../../app/todoSlice';

const TodoItem = ({ id, title, completed }: any) => {
  const [ click, setClick] = useState(false)
  const dispatch = useDispatch()

  const handleCompleteClick = () => {
    setClick(true)
    setTimeout(function() {
      setClick(false)
      dispatch(
        toggleComplete({id: id, completed: !completed})
      )
    }, 2000);
    
  }

  const handleDeleteClick = () => {
    dispatch(
      deleteTodo({id: id})
    )
  }

	return (
		<li className={`list-group-item ${completed && 'list-group-item-success'}`}>
			<div className='d-flex justify-content-between'>
				<span style={{position: 'relative', display: 'flex', justifyContent: 'center', marginRight: click ? 25 : 0, padding: 5}}>
					{click ? <Loading /> : null}<input type='checkbox' className='mr-3' checked={completed} onChange={handleCompleteClick}></input>
					{title}				  
          <DeleteButton handleDeleteClick={handleDeleteClick}/>
				</span>
			</div>
		</li>
	);
};

export default TodoItem;