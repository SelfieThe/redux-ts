import React from "react";
import { makeStyles } from "@mui/styles";
import { CreateChart } from "../../commonComponents/Chart/Chart";
import Grid from "@mui/material/Grid";

const useStyles: any = makeStyles({
    HorizontalCenter: {
        display: "flex",
        justifyContent: "center"
    },
    HorizontalEnd: {
        display: "flex",
        justifyContent: "end"
    },
    Chart: {
        margin: "2px 2px 2px 2px",
    },
    VerticalCenter: {
        display: "flex",
        alignItems: "center"
    },
    ContainerDatePicker: {
        display: 'flex',
        flexWrap: 'wrap',
        marginBottom: "10px",
        justifyContent: 'center'
    },
    TextFieldDatePicker: {
        marginLeft: 1,
        marginRight: 1,
        width: 200,
    },
    ChartRadioGroup: {
        flexDirection:"row", 
        justifyContent:"center"
    },
    Center: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
});

export const Home = () => {
    const Styles = useStyles()
    const Charts = [ 
        {Id: 1, Title: 'Chart1', Request: 1, Get: 'byDate', Group: null, Styles, DatePicker: true, SwitchChart: true, Month: true, TypeChart: 'Multi', CowId: null, ChartLine: {Key: 1, Width: 650, Height: null}},    
    ]

    return (
        <Grid container item xs={12} className={Styles.Center}>
            <Grid item style={{width: 600}} className={Styles.HorizontalCenter}>
                {Charts.map((item: any) => {
                    const data: any = {Id: item.Id, Title: item.Title, Request: item.Request, Get: item.Get, Group: item.Group, styles: item.Styles, DatePicker: item.DatePicker, SwitchChart: item.SwitchChart, Month: item.Month, TypeChart: item.TypeChart, CowId: item.CowId,  ChartLine: item.ChartLine }
                    return (
                        <div key={item.Id}>
                        {CreateChart(data)}
                        </div>
                    )
                })}
            </Grid>
        </Grid>
    )
}