import React, { useState } from "react"
import Grid from "@mui/material/Grid"
import { Link } from "react-router-dom";
import ListAltIcon from '@mui/icons-material/ListAlt';
import ContactPageOutlinedIcon from '@mui/icons-material/ContactPageOutlined';

export const RoutePage = (props:any) => {
    return (
      <Link to={props.href} className={props.active === props.activePage ? "list active" : "list"} onClick={() => props.setActive(props.activePage)}>
        <Grid className="container">
          <span className="icon">
            {props.activePage === 0 ? <ContactPageOutlinedIcon style={{ width: 30, height: 30 }} /> : <></>}
            {props.activePage === 1 ? <ListAltIcon style={{ width: 30, height: 30 }} /> : <></>}
            {props.activePage === 2 ? <ListAltIcon style={{ width: 30, height: 30 }} /> : <></>}
            {props.activePage === 3 ? <ListAltIcon style={{ width: 30, height: 30 }} /> : <></>}
            {props.activePage === 4 ? <ListAltIcon style={{ width: 30, height: 30 }} /> : <></>}
            {props.activePage === 5 ? <ListAltIcon style={{ width: 30, height: 30 }} /> : <></>}          
          </span>
          <span className="text">{props.title}</span>
        </Grid>
      </Link>
    )
  }