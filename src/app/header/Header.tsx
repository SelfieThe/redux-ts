import Grid from "@mui/material/Grid";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./header.css"
import { RoutePage } from "./SetActive";

export const Header = () => {
  const [active, setActive]: any = useState(0) //sessionStorage.getItem('active') ? sessionStorage.getItem('active') :
  
  React.useEffect(() => {
    sessionStorage.setItem("active", active);
  }, [active]);
  
  return (
    <Grid className="navigation">
      
        <RoutePage href="/"      activePage={0} title="Home"  active={active} setActive={setActive}/>
        <RoutePage href="/page"  activePage={1} title="Page"  active={active} setActive={setActive}/>
        <RoutePage href="/page1" activePage={2} title="Page1" active={active} setActive={setActive}/>
        <RoutePage href="/page2" activePage={3} title="Page2" active={active} setActive={setActive}/>
        <RoutePage href="/page3" activePage={4} title="Page3" active={active} setActive={setActive}/>
        <RoutePage href="/page4" activePage={5} title="Page4" active={active} setActive={setActive}/>
        
        <Grid className="indicator"></Grid>
    </Grid>
  )
}

