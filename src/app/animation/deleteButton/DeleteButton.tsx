import { Grid } from "@mui/material";
import React from "react";
import './deleteButton.css'

export const DeleteButton = (props: any) => {
  
  return <>
    <Grid className="btn">
      <a onClick = {props.handleDeleteClick}>Delete</a>
    </Grid>
  </>
}