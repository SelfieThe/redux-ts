import Grid from "@mui/material/Grid";
import React from "react";
import "./loading.css"
import { stylesObj } from './stylesObj'

export const Loading = () => {
  
  return <>
    <Grid style={{position: 'relative', transform: 'translate(23px, 0px)'}}>
      <section>
        <Grid className="loader">          
          <span style={stylesObj.i1}></span>
          <span style={stylesObj.i2}></span>
          <span style={stylesObj.i3}></span>
          <span style={stylesObj.i4}></span>
          <span style={stylesObj.i5}></span>
          <span style={stylesObj.i6}></span>
          <span style={stylesObj.i7}></span>
          <span style={stylesObj.i8}></span>
          <span style={stylesObj.i9}></span>
          <span style={stylesObj.i10}></span>
          <span style={stylesObj.i11}></span>
          <span style={stylesObj.i12}></span>
          <span style={stylesObj.i13}></span>
          <span style={stylesObj.i14}></span>
          <span style={stylesObj.i15}></span>
          <span style={stylesObj.i16}></span>
          <span style={stylesObj.i17}></span>
          <span style={stylesObj.i18}></span>
          <span style={stylesObj.i19}></span>
          <span style={stylesObj.i20}></span>
        </Grid>
      </section>
    </Grid>
  </>
}